type QuestionType = "single_choice_question" | "ordering_question";

type Item = {
  id: string;
  duration: string;
  questiontype: QuestionType;
  question_fi: string;
  maxPoints: number;
  maxAttempts: number;
  totalReachedPoints: number;
  totalSubmitedAnswers: number;
  averagePoints: number;
};

type Participant = {
  active_id: string;
  points: number;
  maxPoints: number;
  questionCount: number;
  answeredQuestions: number;
  workingTime: number;
  timestamp: number;
  test_fi: number;
};

type SummaryData = {
  studentAmount: number;
  itemAmount: number;
  submissionAmount: number;
  maxPoints: number;
  totalPoints: number;
  passedStudents: number;
  failedStudents: number;
  averagePoints: number;
  amountSolvedItems: number;
  testDuration: string;
};

type ExamData = {
  items: Item[];
  summary: SummaryData;
  participants?: Participant[];
};
