export type TstResultCacheEntry = {
  attr: {
    active_fi: string;
    pass: string;
    max_points: string;
    reached_points: string;
    mark_short: string;
    mark_official: string;
    passed: string;
    failed: string;
    timestamp: string;
  };
};
