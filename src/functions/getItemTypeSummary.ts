// Summarizes the items according to their questiontype property
type QuestionType = "single_choice_question" | "ordering_question" | "multiple_choice_question";

type Props = {
  questiontype: QuestionType;
  avgPoints: number;
  totalReachedPoints: number;
  maxPoints: number;
  totalSubmitedAnswers: number;
};

type ItemTypeResult = {
  avgPointsPercentage: number;
  totalSubmitedAnswers: number;
  totalPoints: number;
  totalMaxPoints: number;
  totalItems: number;
};

type ItemTypes = {
  single_choice_question: ItemTypeResult;
  multiple_choice_question: ItemTypeResult;
  ordering_question: ItemTypeResult;
};

export function getItemTypeSummary(items: Props[]): ItemTypes {
  const itemTypes: ItemTypes = {
    single_choice_question: {
      totalSubmitedAnswers: 0,
      avgPointsPercentage: 0,
      totalPoints: 0,
      totalMaxPoints: 0,
      totalItems: 0,
    },
    ordering_question: {
      totalSubmitedAnswers: 0,
      avgPointsPercentage: 0,
      totalPoints: 0,
      totalMaxPoints: 0,
      totalItems: 0,
    },
    multiple_choice_question: {
      totalSubmitedAnswers: 0,
      avgPointsPercentage: 0,
      totalPoints: 0,
      totalMaxPoints: 0,
      totalItems: 0,
    },
  };

  items.forEach((item) => {
    itemTypes[item.questiontype].totalSubmitedAnswers += item.totalSubmitedAnswers;
    itemTypes[item.questiontype].totalPoints += item.avgPoints;
    itemTypes[item.questiontype].totalMaxPoints += item.maxPoints;
    itemTypes[item.questiontype].totalItems += 1;
  });

  itemTypes.single_choice_question.avgPointsPercentage = (itemTypes.single_choice_question.totalPoints / itemTypes.single_choice_question.totalMaxPoints) * 100;
  itemTypes.ordering_question.avgPointsPercentage = (itemTypes.ordering_question.totalPoints / itemTypes.ordering_question.totalMaxPoints) * 100;
  itemTypes.multiple_choice_question.avgPointsPercentage = (itemTypes.multiple_choice_question.totalPoints / itemTypes.multiple_choice_question.totalMaxPoints) * 100;

  return itemTypes;
}
