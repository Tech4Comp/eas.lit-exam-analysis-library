type Result = {
  itemId: string;
  clickPercentage: number;
  clickAmount: number;
  questiontype: QuestionType;
};

type Item = {
  id: string;
  totalSubmitedAnswers: number;
  questiontype: QuestionType;
  answers: [
    {
      shuffled: boolean;
      options: [
        {
          id: string;
          text: string;
          amountSelected: number;
          points: number;
        }
      ];
    }
  ];
};

export function getDistractors(items: Item[]): Result[] {
  const distractors: Result[] = [];

  items.forEach((item) => {
    item.answers.forEach((answer) => {
      answer.options.forEach((option) => {
        // 0 == is a distactor question
        if (option.points === 0) {
          distractors.push({
            itemId: item.id,
            clickPercentage: (option.amountSelected / item.totalSubmitedAnswers) * 100,
            questiontype: item.questiontype,
            clickAmount: option.amountSelected,
          });
        }
      });
    });
  });

  return distractors;
}
