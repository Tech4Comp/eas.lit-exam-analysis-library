type PointDistribution = {
  data: Array<number>;
  labels: Array<number>;
};

type Props = {
  maxpoints: number;
  participants: [
    {
      points: number;
    }
  ];
};

export function getPointDistribution(props: Props): PointDistribution {
  const pointDistribution: PointDistribution = {
    data: [],
    labels: [],
  };
  const maxPoints = props.maxpoints;

  pointDistribution.data = Array(maxPoints).fill(0);
  pointDistribution.labels = Array.from({ length: maxPoints }, (_, index) => index + 1);

  props.participants.forEach((participant) => {
    pointDistribution.data[participant.points]++;
  });

  return pointDistribution;
}
